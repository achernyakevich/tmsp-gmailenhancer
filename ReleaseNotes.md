## Release Notes ##

## v. 0.4.0 ##

### Fixes ###

* Fixed after August/September 2024 update of Gmail UI.

## v. 0.3.3 ##

### Enhancements ###

* Gmail locale detection added. Now script works for any UI language.

## v. 0.3.2 ##

### Fixes ###

* Initiation of snooze Date/Time set first selected option's date/time
automatically without changing selection

## v. 0.3.1 ##

### Features ###

* Possibility to set snooze Date/Time by predefined delay for Snooze popup
* Possibility to set snooze Date/Time for predefined time for Snooze popup

## v. 0.3.0 ##

### Misc ###

* Renaming (GmailUITweaker -> GmailEnhancer)

## v. 0.2.3 ##

### Misc ###

* Migrated to new public repository
* Added metainformation for supporting automatic updates and public activities

## v. 0.2.2 and earlier ##

### Features ###

* Predefined time selector for Snooze popup
* Increasing size of default font (mail view and editing)
